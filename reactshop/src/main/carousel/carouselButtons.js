import React from "react";
import "./carousel.scss";

const CarouselButton = ({ css, button, materialButton }) => {
    return (
        <div className={css}>
            <i className={materialButton}>{button}</i>
        </div>
    )
}

export default CarouselButton;