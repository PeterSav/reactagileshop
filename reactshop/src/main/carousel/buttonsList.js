const buttons = [
    {
        css: "carousel_container__button",
        materialButton: "material-icons md-24",
        button: "arrow_back_ios",
        key: 1
    },
    {
        css: "carousel_container__button",
        materialButton: "material-icons md-18",
        button: "lens",
        key: 2
    },
    {
        css: "carousel_container__button",
        materialButton: "material-icons md-18",
        button: "lens",
        key: 3
    },
    {
        css: "carousel_container__button",
        materialButton: "material-icons md-18",
        button: "lens",
        key: 4
    },
    {
        css: "carousel_container__button",
        materialButton: "material-icons md-24",
        button: "arrow_forward_ios",
        key: 5
    }
]

export default buttons;