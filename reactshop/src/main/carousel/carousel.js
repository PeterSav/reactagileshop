import React from "react";
import s10 from "../../../src/header/headerComponents/imgs/s10.jpg";
import "./carousel.scss";
import CarouselButton from "./carouselButtons";
import buttons from "./buttonsList";

class Carousel extends React.Component {

    createButton = button => {
        return (
            <CarouselButton
                css={button.css}
                materialButton={button.materialButton}
                button={button.button}
                key={button.key}
            />
        )
    }

    render() {
        return (
            <div className="carousel_container" >
                <figure>
                    <img src={s10} alt="s10" />
                    <figcaption className="carousel_container__main_shot_caption">
                        The next generation takes shape
                    </figcaption>
                </figure>
                {buttons.map(button => this.createButton(button))}
            </div>
        )
    }
}

export default Carousel;