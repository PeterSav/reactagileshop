import React from "react";
import Carousel from "./carousel/carousel";
import Headline from "./headline/headline";
import Product from "./products/product";
import "./main.scss";

export default class Main extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            products: []
        }
    }

    fetchDevices = async () => {
        const URL = `https://cors-anywhere.herokuapp.com/https://gitlab.com/IsminiD/devices-json/raw/master/devices.json`;
        const fetchResult = fetch(URL)
        const response = await fetchResult;
        const jsonData = await response.json();
        let resData = jsonData.devices;
        return resData;
    }

    fetchData = async () => {
        const devices = await this.fetchDevices();
        this.setState({ products: devices });

    }

    componentDidMount() {
        this.fetchData();
    }

    getProduct = (products, productType) => {
        return products.filter(product => product.type === productType);
    }

    getAccessories = (products) => {
        return products.filter(product => product.type !== 'smartphone' && product.type !== 'tablet');
    }


    createProduct = product => {
        return (
            <Product
                name={product.name}
                description={product.description}
                price={product.price}
                image={product.image}
                key={product.name}
            />
        )
    }

    createSection = (products, productsType) => {
        return (
            <div>
                <Headline
                    numberOfProducts={products.length}
                    typeOfProducts={productsType}
                />
                <div className="container">
                    {products.slice(0, 3).map(product => this.createProduct(product))}
                </div>
            </div>
        )
    }

    render() {
        const smartphones = this.getProduct(this.state.products, "smartphone");
        const tablets = this.getProduct(this.state.products, "tablet");
        const accessories = this.getAccessories(this.state.products);
        return (
            <div>
                <Carousel />
                {this.createSection(smartphones, "Smartphones")}
                {this.createSection(tablets, "Tablets")}
                {this.createSection(accessories, "Accessories")}
            </div>
        )
    }
}