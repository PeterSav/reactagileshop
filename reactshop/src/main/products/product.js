import React from "react";
import "./product.scss";

const Product = ({ name, description, price, image }) => {
    return (
        <article className="product">
            <figure className="product__photo">
                <button className="product__favorite_button">
                    <i className="material-icons">favorite_border</i>
                </button>
                <div className="product__photo_box">
                    <h3 className="product__title">
                        {name}
                    </h3>
                    <p className="product__subtitle">
                        {description}
                    </p>
                    <span className="product__discover">
                        Discover More
                    </span>
                    <div className="product__price">
                        ${price}
                    </div>
                    <div className="product__ex_price">
                        <span className="product__ex_price_line"></span>
                        $1000
                    </div>
                </div>
                <img src={image} alt={name} className="product__image" />
            </figure>
        </article>
    )
}

export default Product;