import React from "react";
import "./headline.scss";

const Headline = ({ typeOfProducts, numberOfProducts }) => {
    return (
        <div className="section_of_products">
            <div>
                <h3 className="section_of_products__type">
                    {typeOfProducts}
                    <span className="section_of_products__quantity">
                        ({numberOfProducts})
                    </span>
                </h3>
            </div>
            <aside>
                <i className=" material-icons magenta">chevron_left</i>
                <i className="material-icons magenta">chevron_right</i>
                <i className="fas fa-tasks magenta"></i>
            </aside>
        </div>
    )
}

export default Headline;