import React from "react";
import "./footer.scss"
import banner from "./banner.png";

const Footer = () => {
    return (
        <div className="banner">
            <img src={banner} alt="banner" className="banner__size" />
            <footer className="footer">
                <h5 className="footer__copyright"> copyright © Agile Academy 2019 </h5>
            </footer>
        </div>
    )
};

export default Footer;