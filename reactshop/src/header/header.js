import React from "react";
import "./header.scss";
import logo from "./headerComponents/imgs/logo.png";
import SimpleNavButton from "./headerComponents/simpleNavButton/simpleNavButton";
import navButtonsToMap from "./headerComponents/simpleNavButton/navButtonsToMap";
import NavDropDownButton from "./headerComponents/navDropDownButton/navDropDownButton";

export default class Header extends React.Component {

    createNavButton = item => {
        return <SimpleNavButton
            buttonName={item}
            key={item}
        />
    }

    render() {
        return (
            <header className="header">
                <div className="header__container">
                    <a href="./index.html" className="header__container_logo">
                        <img src={logo} alt="store logo" />
                    </a>
                    <nav>
                        {navButtonsToMap.map(item => this.createNavButton(item))}
                        <NavDropDownButton
                            type="cart"
                        />
                        <NavDropDownButton
                            type="profile"
                        />
                    </nav>
                </div>
            </header>
        )
    }
}