import React from "react";
import Wording from "../../../helpers/commonComponents/wording";
import "./navDropDownButton.scss";

const NavDropDownButton = ({ type }) => {

    if (type === "cart") {
        return (
            <span className="header__navDropDown">
                <i className="fas fa-shopping-cart header__icon_align"></i>
                <span className="header__cartNumber">
                    2
        </span>
                <div className="header__cartDropDown">
                    <Wording
                        message="You have 2 items in your cart"
                        className="topBottomPad"
                    />
                    <Wording
                        message="total cost:"
                        className="header__costText"
                    />
                    <Wording
                        message="$1259"
                        className="header__costNumber"
                    />
                    {/* <p className="header__costNumber">$1259</p> */}
                    <button className="header__checkoutButton">
                        <i className="material-icons header__icon_align">chevron_right</i>
                        CHECKOUT
                </button>
                    <button className="header__myCartButton">
                        <i className="material-icons header__icon_align">shopping_cart</i>
                        MY CART
                </button>
                </div>
            </span>
        );
    }
    else if (type === "profile") {
        return (
            <span className="header__navDropDown">
                <i className="fas fa-user-alt header__icon_border"></i>
                <div className="header__profileDropDown">
                    {/* <p className="header__newUserText">hello, user</p> */}
                    <Wording
                        className="header__newUserText topBottomPad"
                        message="hello, user"
                    />
                    <button className="header__registerButton">
                        <i className="material-icons header__icon_align">account_box</i>
                        REGISTER
                        </button>
                    <button className="header__signInButton">
                        <i className="material-icons header__icon_align">send</i>
                        SIGN IN
                        </button>
                </div>
            </span>
        )
    }
}

export default NavDropDownButton;