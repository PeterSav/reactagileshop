import React from "react";
import "./simpleNavButton.scss";

const SimpleNavButton = ({ buttonName }) => {
    return (
        <a href="#index" className="header__navButton">{buttonName}</a>
    );
}

export default SimpleNavButton;