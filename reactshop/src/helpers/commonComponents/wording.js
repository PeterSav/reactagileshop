import React from "react";

const Wording = ({ message, className = '' }) => {
    return (
        <div className={className}>{message}</div>
    );
}

export default Wording;